;;; meshphone_gen.php  Release: VERSION
;;; Copyright (c)2024 Gerard Hickey under GNU GPLv3 license

; This is an include file that contains the 7-digit MeshPhone
; routing guide template. Routing instructions can be as broad or
; as specific as appropriate for your environment.

; Entrance into this context is via the Access Code 78 (local user)
; or incoming MeshPhone call from another MeshPhone PBX.

; Traffic flow for this context can be in one of four directions:

; 1. A local user who dials access code 78 (outbound). This call
; exits via a MeshPhone trunk to another MeshPhone PBX or via the
; Sorry message.

; 2. Inbound traffic from a trunk from another MeshPhone PBX. This
; traffic terminates on a local extension.

; 3. Inbound traffic from a trunk from another MeshPhone PBX. This
; traffic exits via a Meshphone trunk to another MeshPhone PBX.
; (This traffic is called "Transit" or "Tandem" traffic.)

; 4. Traffic with an incorrect number. This traffic exits to the
; Sorry message which alerts the caller that there is a problem with
; the number as dialed. An "Office Code" is also given which can
; aid in the diagnosis of the dialing problem.

[MeshPhone]

<?php
const DEFAULT_ROUTABLE_STATUS = array('Active', 'Inactive', 'Provisioning');

// Load config file (override with local meshphone.ini for testing)
$config_file = file_exists('meshphone.ini') ? "meshphone.ini" : "/usr/local/meshphone/meshphone.ini";
$config = parse_ini_file($config_file, true, INI_SCANNER_RAW);
$data_source = $config["options"]["meshphone_db_url"] ?? "http://wt0f-meshphone.local.mesh/api/network_json";

// Include custom extensions if directed to
if (($config["options"]['custom_dialplan'] ?? '') == 'before' &&
    file_exists('/etc/asterisk/meshphone_custom.conf')) {
    echo "#include \"/etc/asterisk/meshphone_custom.conf\"\n\n";
}

// Load XML file (override with local meshphone_xml.xml for testing)
if (file_exists("network.json")) {
    $data = json_decode(file_get_contents("network.json"), true);
} else {
    $contents = file_get_contents($data_source);
    if ($contents == "") {
        # if we got nothing, then signal that file should not be replaced
        exit(1);
    }
    $data = json_decode($contents, true);
}

if ($data) {
    // index the office codes
    $office_codes = array_combine(
        array_map(function (array $value) {
            return $value['office_codes'][0]['office_code'] ?? null;
        }, $data),
        array_map(function (array $value) {
            return $value['name']; }, $data));

    // index the PBX data
    $pbxes = array_combine(
        array_map(function (array $value) {
            return $value['name']; }, $data),
        array_map(function (array $value) {
            return $value; }, $data));

    // index the IP addresses
    $ips = array_combine(
        array_map(function (array $value) {
            return strval($value['ip_address'] ?? null);
        }, $data),
        array_map(function (array $value) {
            return $value['name'] ?: null; }, $data));

    $routeable_status = array_merge(DEFAULT_ROUTABLE_STATUS,
        explode(':', $config['options']['include_status'] ?? ''));
    // make a place to record the NPA extensions if necessary
    $npa_extens = array();

    // Start the route calculations by loading this PBX into the pbx_ring
    $trunk_names = array();
    if (($config['office_code'] ?? false) && ($config['pbx_name'] ?? false)) {
        error("office_code and pbx_name can not coexist in INI file");
        exit(1);
    } elseif ($config['office_code'] ?? false) {
        $pbx_home = $office_codes[$config['office_code']];
        $pbx_ring = array($pbx_home);
        // add hostnames to config[trunks] to be found later
        foreach ($config['trunks'] ?? [] as $office_code => $name) {
            $config['trunks'][$office_codes[strval($office_code)]] = $name;
            // index the trunk names to PBX names
            $trunk_names[$name] = $office_codes[$office_code];
        }
    } elseif ($config['pbx_name'] ?? false) {
        $pbx_home = strtoupper($config['pbx_name']);
        $pbx_ring = array($pbx_home);
        // insure that config[trunks] reference upper case PBX names
        foreach ($config['trunks'] ?? [] as $pbx_name => $trunk_name) {
            if (!ctype_upper($pbx_name)) {
                $config['trunks'][strtoupper($pbx_name)] = $trunk_name;
            }
        }
        // index the trunk names to PBX names
        $trunk_names = array_flip($config['trunks']);
    }

    $pbxes_visited = array();
    echo ";;; Calculating route costs from PBX {$pbx_ring[0]}\n";

    // record office codes and which trunk they should route through
    $trunks = array($pbx_ring[0] => "nil");
    $distance = 0;
    while ($pbx_ring) {
        $pbx_to_visit = array();

        foreach ($pbx_ring as $entry) {
            $pbx_name = strval($entry);

            // check to see if the PBX has a status that is routeable
            // only skip check if checking *this* PBX
            if ($pbx_name != $pbx_home && !in_array($pbxes[$pbx_name]['pbx_status'], $routeable_status)) {
                continue;
            }

            try {
                // PBX not visited yet. add hop count and collect next ring of PBXes
                if (!($pbxes[$pbx_name]['cost'] ?? false)) {
                    // echo "updating " . $office_code ."\n";
                    $pbxes[$pbx_name]["cost"] = $distance;

                    $pbx_to_visit = array_unique(
                        array_merge($pbx_to_visit, array_map(
                            function (array $trunk) {
                                return $trunk['pbx_name'];
                            }, $pbxes[$pbx_name]['trunks'])));

                    $pbxes_visited[] = $pbx_name;

                    // iterate through the trunks to find the correct trunk to path through
                    // Conditions:
                    //    incoming trunk does not exist, processing home office code
                    //          or trunk to next PBX in the path
                    //    incoming trunk exists and thus leads to home office code

                    // only process if trunks are present
                    foreach ($pbxes[$pbx_name]["trunks"] as $remote) {
                        if (!($trunks[$remote['pbx_name']] ?? false)) {
                            if ($pbx_name == $pbx_home) {
                                // trunk on this PBX
                                $trunks[$remote['pbx_name']] = $remote['pbx_name'];
                            } elseif ($remote['pbx_name'] != $pbx_home) {
                                // trunk to another PBX
                                $trunks[$remote['pbx_name']] = $trunks[$pbx_name];
                            }
                        }
                    }
                }
            } catch (TypeError $e) {
                error("PBX {$pbx_name} specified in trunk does not exist");
            }
        }

        // filter out any PBXes that have already been seen
        $pbx_to_visit = array_diff($pbx_to_visit, $pbxes_visited);

        $pbx_ring = $pbx_to_visit;
        $distance++;
    }

    // Start the building dialplan
    echo ";;; Building dialing plan \n";
    $pbx_ring = array($pbx_home);
    $pbxes_visited = array();
    $dialplans_seen = array("dummy");
    $local_npa = $pbxes[$pbx_home]['office_codes'][0]['npa'] ?? null;

    while ($pbx_ring) {
        // List of PBXes that we discover that have not been visited yet
        $pbx_to_visit = array();

        // Walk through each "PBX ring" (hops away from local PBX) finding
        // PBXes that need to be added to the dialplan
        foreach ($pbx_ring as $pbx_name) {
            try {
                // check for local extension dialplan
                foreach ($pbxes[$pbx_name]['office_codes'] as $office) {
                    $local_extens = $pbx_name == $pbx_home;
                    $pbx_info = $pbxes[$pbx_name];
                    render_pbx_info($pbx_info);

                    // PBX being examined has dialplans attached
                    if ($office['dialplans'] ?? false) {
                        foreach ($office["dialplans"] as $pattern) {
                            if (!($dialplans_seen[$pattern] ?? false)) {
                                if ($local_extens) {
                                    // add local extensions to the dialplan
                                    render_local($pattern);
                                } else {
                                    // verify that the trunk exists
                                    if (!($config['trunks'][$trunks[$pbx_name]] ?? false)) {

                                        error("PBX does not have trunk for {$pbx_name} in config file, " .
                                              "dropping {$pattern} from the dialplan");
                                    } else {
                                        // add route to dialplan through trunk
                                        render_route($pattern, $config['trunks'][$trunks[$pbx_name]]);

                                        // check to see if we have npa_extensions turned on
                                        if ($config["options"]['npa_extensions'] ?? false) {
                                            if (substr($pattern, 1, 3) == $local_npa) {
                                                // dialplan exists in the same NPA
                                                $npa_extens[$pattern] = $config['trunks'][$trunks[$pbx_name]];
                                            }
                                        }
                                    }
                                }
                            }
                            $dialplans_seen[] = $pattern;
                        }
                    }
                    echo "; Last updated: {$pbx_info["updated_at"]}\n\n";
                }

                $pbx_to_visit = array_unique(
                    array_merge($pbx_to_visit, array_map(
                        function (array $trunk) {
                            return $trunk['pbx_name'];
                        }, $pbxes[$pbx_name]['trunks'])));

                $pbxes_visited[] = $pbx_name;
            } catch (TypeError $e) {
                echo "{$e}\n";
                error("{$pbx_name} specified in trunk does not exist");
            }
        }

        // filter out any PBXes that have already been seen
        $pbx_to_visit = array_diff($pbx_to_visit, $pbxes_visited);

        $pbx_ring = $pbx_to_visit;

    }
} else {
    // signal that we could not retrieve the XML
    critical("JSON data source could not be retrieved");
    exit(1);
}

// Include custom extensions if directed to
if (($config["options"]['custom_dialplan'] ?? '') == 'after' &&
    file_exists('/etc/asterisk/meshphone_custom.conf')) {
    echo "\n#include \"/etc/asterisk/meshphone_custom.conf\"\n";
}

// checkin with wt0f-meshphone unless told not to
if (!($config['options']['no_checkin'] ?? false)) {
    $url = 'http://wt0f-meshphone.local.mesh/api/checkin';
    $interface = $config['options']['checkin_interface'] ?? null;
    $data = ['pbx_name' => $pbx_home,
            'hostname' => gethostname(),
            'ip_address' => find_ip($interface),
            'pbx_type' => identify_pbx_type(),
            'gen_script_name' => 'wt0f_meshphone_generators',
            'gen_script_version' => 'VERSION', ];

    $my_trunks = array_map(function (array $def) {
        return $def['pbx_name']; }, $pbxes[$pbx_home]['trunks']);
    exec('rasterisk -x "iax2 show peers"', $output, $ret);
    if ($ret == 0) {
        $trunks = array();
        $line_re = "/^(\w+)\/\w+\s+\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}.*4569\s+(?:\(T\))?\s+(\w+)\s?(?:\((\d+) ms\))?/";
        foreach ($output as $line) {
            preg_match($line_re, $line, $match);
            if ($match) {
                $remote = $trunk_names[$match[1]] ?? null;
                if ($remote && in_array($remote, $my_trunks)) {
                    if ($match[2] == "OK") {
                        $trunks[] = [ 'pbx_name' => $remote, 'latency' => $match[3] ];
                    } elseif ($match[2] = "UNREACHABLE") {
                        $trunks[] = [ 'pbx_name' => $remote, 'latency' => "Unreachable" ];
                    }
                }
            }
        }

        if ($trunks) {
            $data['trunks'] = $trunks;
        }
    }

    echo "\n";
    echo ";;; Sending checkin data to {$url}\n";
    echo ";;;       pbx_name: {$data['pbx_name']}\n";
    echo ";;;       hostname: {$data['hostname']}\n";
    echo ";;;       ip_addr:  {$data['ip_address']}\n";
    echo ";;;       pbx_type: {$data['pbx_type']}\n";
    echo "\n";

    // use key 'http' even if you send the request to https://...
    $options = [
        'http' => [
            'header' => "Content-type: application/x-www-form-urlencoded\r\n",
            'method' => 'POST',
            'content' => http_build_query($data),
        ],
    ];

    $context = stream_context_create($options);
    @file_get_contents($url, false, $context);
}

function error(string $text) {
    echo "; ERROR: $text\n";
}

function critical(string $text) {
    echo "; CRITICAL: $text\n";
}

function warning(string $text) {
    echo ";;; WARNING: $text\n";
}

function render_pbx_info($info) {
    echo "\n; PBX: {$info["name"]} // {$info["location"]} admin: {$info["admin_name"]}, " .
         "{$info["admin_callsign"]}, {$info["admin_phone"]}, {$info["admin_email"]}\n";
}

function render_local(string $pattern) {
    echo "exten => {$pattern},1,GoTo(from-internal,\${EXTEN:3},1)        ;local extensions\n";
}

function render_route(string $pattern, string $trunks, string $prefix='') {
    $first = true;
    foreach (explode('|', $trunks) as $trunk) {
        if ($first) {
            echo "exten => {$pattern},1,Dial(IAX2/{$trunk}/{$prefix}\${EXTEN})\n";
            $first = false;
        } else {
            echo "exten => {$pattern},n,Dial(IAX2/{$trunk}/{$prefix}\${EXTEN})\n";
        }
    }
    echo "exten => {$pattern},n,GoTo(Utilities,Sorry,1)\n";
}

function identify_pbx_type() {
    if (file_exists('/etc/asterisk/rpt.conf')) {
        return 'Allstar';
    } elseif (file_exists('/etc/amportal.conf')) {
        $amportal_contents = file_get_contents("/etc/amportal.conf");
        $freepbx_magic = "BRAND_IMAGE_FREEPBX_FOOT=images/freepbx_small.png";
        $incrpbx_magic = "BRAND_IMAGE_FREEPBX_LINK_FOOT=http://incrediblepbx.com/";
        if (strpos($amportal_contents, $freepbx_magic) !== false) {
            if (strpos(file_get_contents('/proc/cpuinfo'), 'BCM2708') !== false) {
                return 'RasPBX';
            } else {
                return 'FreePBX';
            }
        } elseif (strpos($amportal_contents, $incrpbx_magic) !== false) {
            return 'IncrediblePBX';
        }
    }
}

function find_ip($interface = null) {
    if ($interface == null) {
        exec('hostname -I', $ip_address);
        if (substr_count($ip_address[0], ' ') > 0) {
            error("IP address can not be gathered. Need to use checkin_interface option");
        } else {
            return $ip_address[0];
        }
    } else {
        exec("ip -f inet addr show $interface 2>/dev/null | awk '/inet / {print $2}'", $ip_address);
        if ($ip_address) {
            $ip_address = substr($ip_address[0], 0, -3);
            return $ip_address;
        } else {
            warning("checkin_interface option set to non-existent interface");
        }
    }
}

echo "; Catch any non-routeable MeshPhone numbers\n";
echo "exten => _xxxxxxx,1,GoTo(Utilities,Sorry,1)\n";

// produce dialplan for NPA extensions
if ($npa_extens) {
    echo "\n;;; Creating dialplan for NPA extensions\n";
    echo "[from-internal]\n\n";
    echo "; extensions enabled within the same NPA\n";

    foreach ($npa_extens as $pattern => $trunk) {
        $npa = substr($pattern, 1, 3);
        $pattern = '_' . substr($pattern, 4);
        render_route($pattern, $trunk, $npa);
    }
}
?>

; Notes

; 1. Include this file into your extensions.conf (or
; extensions_custom.conf for FreePBX) with a #include statement.
; e.g.

; #include "/etc/asterisk/meshphone.conf"

; 2. Area code 533 and 577 are defined as a "non-geograpic" area
; codes. They are typically used for end offices that do not have
; a fixed location. Thus, it is used  here for mobile pbx's,
; Go-Kits, and Hamshack Hotline.

; https://www.nationalnanpa.com/enas/npa_query.do

; (Enter "533" or "577" into the search box.)

; Powered by meshphone_gen. https://gitlab.com/aredn-apps/meshphone-generators/

; End of include file
