<?php
$header = <<<EOH
<?xml version="1.0" encoding="UTF-8" standalone="yes"?>
<!-- polycom_directory_gen.php  Release: VERSION  -->
<!-- Copyright (c)2024 Gerard Hickey under GNU GPLv3 license -->
<directory>
    <item_list>
EOH;
echo $header;

// Load config file (override with local meshphone.ini for testing)
$config_file = file_exists('meshphone.ini') ? "meshphone.ini" : "/usr/local/meshphone/meshphone.ini";
$config = parse_ini_file($config_file, true);
$data_source = $config["options"]["whitepages_data_url"] ?? "http://n2mh-meshphone2.local.mesh/meshphone/wpxml.xml";

// Load XML file
$contents = file_get_contents($data_source);
if ($contents == "") {
    # if we got nothing, then signal that file should not be replaced
    exit(1);
}
$xml = simplexml_load_string($contents);

if ($xml) {
    $seen = array();
    foreach ($xml->listing as $entry) {
        $callsign = strval($entry->callsign);
        $phone_num = sanitize(strval($entry->meshphone));
        if (!in_array($callsign, $seen)) {
            # Only generate an entry if we have not done so yet
?>
        <item>
            <ln><?php print $callsign ?></ln>
            <fn><?php print strval($entry->opname)?></fn>
            <ct>77<?php print $phone_num ?></ct>
            <rt>3</rt>
            <dc/>
            <ad>0</ad>
            <ar>0</ar>
            <bw>0</bw>
            <bb>0</bb>
        </item>
<?php
            $seen[] = $callsign;
        }
    }
}

function sanitize($number) {
    $letter_map = array(
        '2' => '/[abc]/i', '3' => '/[def]/i', '4' => '/[ghi]/i', '5' => '/[jkl]/i',
        '6' => '/[mno]/i', '7' => '/[pqrs]/i', '8' => '/[tuv]/i', '9' => '/[wyxz]/i',
    );
    $number = str_replace('-', '', $number);

    // Convert any letters to digits
    foreach ($letter_map as $digit => $pattern) {
        $number = preg_replace($pattern, $digit, $number);
    }
    return $number;
}
?>
    </item_list>
</directory>
