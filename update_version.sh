#!/bin/bash

version=$1

sed  -i -E "s/(Release:\s*|=>\s*')[VERSION0-9.]+/\1$version/" meshphone_gen.php
sed  -i -E "s/Release: *[VERSION0-9.]+/Release: $version/" callsign_gen.php
sed  -i -E "s/Release: *[VERSION0-9.]+/Release: $version/" polycom_directory_gen.php

