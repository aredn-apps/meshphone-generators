

## [2.3.2](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.3.1...v2.3.2) (2024-06-22)


### Bug Fixes

* Use trunk name to identify PBX on IAX trunk ([d27b00f](https://gitlab.com/aredn-apps/meshphone-generators/commit/d27b00f8f662729710ccb5d0fdc377c27c3c2d58))

## [2.3.1](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.3.0...v2.3.1) (2024-06-11)


### Bug Fixes

* Add support for gen_script_name/version ([30652fd](https://gitlab.com/aredn-apps/meshphone-generators/commit/30652fd6ff29ea95152243241637c87e52c46690))
* update_version capturing text ([1565597](https://gitlab.com/aredn-apps/meshphone-generators/commit/1565597648866af428410e852d788750327b06cb))

## [2.3.0](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.2.2...v2.3.0) (2024-05-26)


### Features

* Add support for multi interface during checkin ([29b01f0](https://gitlab.com/aredn-apps/meshphone-generators/commit/29b01f0423f073c38718b8a3b4dd027e345bee23))

## [2.2.2](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.2.1...v2.2.2) (2024-05-13)


### Bug Fixes

* Detect when trunk is in unreachable state ([55deaf4](https://gitlab.com/aredn-apps/meshphone-generators/commit/55deaf4bba7e7abb0fcbbf4e4f84e0e6f186e859))

## [2.2.1](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.2.0...v2.2.1) (2024-05-06)


### Bug Fixes

* Patching NPA extensions to dial full MeshPhone number ([2dc8469](https://gitlab.com/aredn-apps/meshphone-generators/commit/2dc84696fe0890f67cfca2ac2994a71adc5e5f46))

## [2.2.0](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.6...v2.2.0) (2024-05-05)


### Features

* Add support for multi trunk connections ([f2bff8a](https://gitlab.com/aredn-apps/meshphone-generators/commit/f2bff8a835cd2dc8d8d53ea1d3a24dd48ce1c4d8))


### Bug Fixes

* Fix context for NPA extensions ([6b5f42a](https://gitlab.com/aredn-apps/meshphone-generators/commit/6b5f42ab31711e8743218295a6d9a1c7ba681fef))
* Handle when PBX names are lower case ([dd191f8](https://gitlab.com/aredn-apps/meshphone-generators/commit/dd191f87371256de8cbefc24a01ebc295f7625b6))

## [2.1.6](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.5...v2.1.6) (2024-04-28)


### Bug Fixes

* Allow PBX to be in maintenance and still produce a dialplan ([16a960e](https://gitlab.com/aredn-apps/meshphone-generators/commit/16a960e2f988595dd5177c64b5b6ee67fde9caf5))

## [2.1.5](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.4...v2.1.5) (2024-04-28)


### Bug Fixes

* Fixed NPA extension when using DB 2.0 ([4a76d25](https://gitlab.com/aredn-apps/meshphone-generators/commit/4a76d252221bddeb239719afe04eff0d796bdd5d))

## [2.1.4](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.3...v2.1.4) (2024-04-26)


### Bug Fixes

* Add protection around possibly missing keys in PBX data ([8f77211](https://gitlab.com/aredn-apps/meshphone-generators/commit/8f772111010157d298065c73d060b18c834d44ac))

## [2.1.3](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.2...v2.1.3) (2024-04-21)


### Bug Fixes

* Load config file in callsign_gen.php ([864f624](https://gitlab.com/aredn-apps/meshphone-generators/commit/864f624d82ebbe49bea64f687997e41ca236922a))

## [2.1.2](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.1...v2.1.2) (2024-04-20)


### Bug Fixes

* Fix routing through unavailable trunks ([3d769c6](https://gitlab.com/aredn-apps/meshphone-generators/commit/3d769c6ba6863e8e531e4baeb6119223f35d652a))

## [2.1.1](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.1.0...v2.1.1) (2024-04-20)


### Bug Fixes

* Fix [#include](https://gitlab.com/aredn-apps/meshphone-generators/issues/include) for early versions of Asterisk ([6f91ae2](https://gitlab.com/aredn-apps/meshphone-generators/commit/6f91ae2f9617acfc742eb2610ad07a50016917d2))

## [2.1.0](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.0.1...v2.1.0) (2024-04-20)


### Features

* Add checkin to DB and collection of trunk latencies ([5696ced](https://gitlab.com/aredn-apps/meshphone-generators/commit/5696ced3d21683c999722a8a7e36bdb6cfa6146f))
* Add filtering to PBX selection ([bbaf614](https://gitlab.com/aredn-apps/meshphone-generators/commit/bbaf6147f3d2531e0dfce0638d9ea0dda01cd9bb))
* Add support for including callsign_custom.conf ([3726e38](https://gitlab.com/aredn-apps/meshphone-generators/commit/3726e3813e616d690d80be30670447e7af1b35af))


### Bug Fixes

* Add release version numbering to scripts ([34a12c0](https://gitlab.com/aredn-apps/meshphone-generators/commit/34a12c0053815b054c818e58ac3a9e5600e2edcf))

## [2.0.1](https://gitlab.com/aredn-apps/meshphone-generators/compare/v2.0.0...v2.0.1) (2024-04-11)


### Bug Fixes

* Corrected lookup of PBX name when office codes are used ([50ef9e3](https://gitlab.com/aredn-apps/meshphone-generators/commit/50ef9e33aabd229ba4e30e0f1da16b77b38d480e))

## [2.0.0](https://gitlab.com/aredn-apps/meshphone-generators/compare/v1.2.1...v2.0.0) (2024-04-08)


### ⚠ BREAKING CHANGES

* In addition to pulling data from MeshPhone DB 2.0, 
the data source has been changed to a JSON data source. There are 
changes to meshphone.ini. The preference is to use PBX names instead
of office codes in meshphone.ini. Use of office codes is now deprecated
and will be removed in a future version. 

### Features

* Update scripts for MeshPhone DB 2.0 ([b367d3d](https://gitlab.com/aredn-apps/meshphone-generators/commit/b367d3d936bcfe00a982627f91a61b74a46f9f20))

## [1.2.1](https://gitlab.com/aredn-apps/meshphone-generators/compare/v1.2.0...v1.2.1) (2024-03-23)


### Bug Fixes

* Corrected paths in README for the scripts ([04179ba](https://gitlab.com/aredn-apps/meshphone-generators/commit/04179babd5fd918aa3a8655ba5d7334a41392c9d))
* Fixed comment character in ini sample file ([cda859a](https://gitlab.com/aredn-apps/meshphone-generators/commit/cda859afe40003dcf7aba9efca8ba58b97a95bb9))
* New cron scripts for portability ([bffff24](https://gitlab.com/aredn-apps/meshphone-generators/commit/bffff242ebd00a121fdfdcfc2cd6e02396a5758f))

## [1.2.0](https://gitlab.com/aredn-apps/meshphone-generators/compare/v1.1.0...v1.2.0) (2024-03-04)


### Features

* Add Polycom directory generator script ([aba5ff5](https://gitlab.com/aredn-apps/meshphone-generators/commit/aba5ff597533287ad482a8e2edb15113333ed4bb))


### Documentation

* updated installation to use release branch ([e675c82](https://gitlab.com/aredn-apps/meshphone-generators/commit/e675c82b98b50869e67e451bfecfc9af93e1f957))

## [1.1.0](https://gitlab.com/aredn-apps/meshphone-generators/compare/v1.0.0...v1.1.0) (2024-02-27)


### Features

* support for custom dialplans ([9deb26b](https://gitlab.com/aredn-apps/meshphone-generators/commit/9deb26bf1fb7629fdce5013079bc8e7fe8ab3472))


### Documentation

* add note about trunks and updating db ([64dbfc7](https://gitlab.com/aredn-apps/meshphone-generators/commit/64dbfc7efd816cf921737d8b0e77401323470a67))

## 1.0.0 (2024-02-26)


### Features

* add 4 digit dialing within the same NPA ([212ac2a](https://gitlab.com/aredn-apps/meshphone-generators/commit/212ac2a26faf1b3f88f79513f8d727b30c5a9780))
* add consistancy checks of XML data ([a6ad996](https://gitlab.com/aredn-apps/meshphone-generators/commit/a6ad9967461c7c65c17a362d45ee0ff3b34ac34a))
* allow overrides for local testing ([5f7cc14](https://gitlab.com/aredn-apps/meshphone-generators/commit/5f7cc141a304a22a00930357e655434751fa6fa7))
* first version of call sign generator ([590469c](https://gitlab.com/aredn-apps/meshphone-generators/commit/590469c416edee514762eaffe5cc9aba93db42ae))
* simple MeshPhone routing ([5fd386e](https://gitlab.com/aredn-apps/meshphone-generators/commit/5fd386e867bc95b98834b50b80d31162304361c7))


### Bug Fixes

* combine multiline comment into single line ([180a050](https://gitlab.com/aredn-apps/meshphone-generators/commit/180a05015b5f70d55e79cf55478c908b86345596))
* corrected placing call into MeshPhone dialplan ([54e0222](https://gitlab.com/aredn-apps/meshphone-generators/commit/54e02224f301ed09e0918045981b2ee22765ad2b))
* fixed trunk definition in meshphone.ini ([60cc7c2](https://gitlab.com/aredn-apps/meshphone-generators/commit/60cc7c26403438f96c0b1391da53cf05766c58f5))
* handle when office code has not been defined ([d28373a](https://gitlab.com/aredn-apps/meshphone-generators/commit/d28373a127aec82514a65a5efbe0687e74961ffb))
* handle when originating office not defined ([808feba](https://gitlab.com/aredn-apps/meshphone-generators/commit/808feba21c1d966e635de8fc7f9d00c60808e650))
* report error when not able to pull XML data ([a663137](https://gitlab.com/aredn-apps/meshphone-generators/commit/a66313771159f6524cd7b6e8324bc87fb0718341))


### Documentation

* add redirect of stderr on meshphone_gen example ([8e1b6d4](https://gitlab.com/aredn-apps/meshphone-generators/commit/8e1b6d44b2800b3c576d65792996a97e2556e240))
* more detail of parseing XML file ([9b0ce93](https://gitlab.com/aredn-apps/meshphone-generators/commit/9b0ce939d3d76cf7c1d898734bb9b67828922983))
