<?php

// Load XML file (override with local meshphone_xml.xml for testing)
if (file_exists("meshphone_xml.xml")) {
    $xml = simplexml_load_file("meshphone_xml.xml");
} else {
    $contents = file_get_contents("http://n2mh-web.local.mesh/mpadmin/meshphone_xml.xml");
    if ($contents == "") {
        # if we got nothing, then signal that file should not be replaced
        exit(1);
    }
    $xml = simplexml_load_string($contents);
}

$pbxes = array();

if ($xml) {
    echo ";;; Parsing MeshPhone XML data source\n";

    // find all the PBX entries in the XML file
    echo ";;;\tScanning for office codes\n";
    $office_code_index = array();
    $index = 0;
    foreach ($xml->pbx as $entry) {
        $office_code = strval($entry->office_code);
        $ip = strval($entry->ip_address);
        if (! $ip) {
            $ip = $office_code;
        }
        $hostname = $office_code;
        if ($ip) {
            $output = shell_exec("host $ip");
            if (!str_contains($output, "NXDOMAIN")) {
                $parts = explode(" ", $output);
                $hostname = end($parts);
            }
        }

        $pbxes[] = array(
            "name"           => $office_code,
            "location"       => strval($entry->location),
            "longitude"      => strval($entry->lon),
            "latitude"       => strval($entry->lat),
            "network"        => strval($entry->network),
            "admin_callsign" => strval($entry->owner_callsign),
            "admin_phone"    => strval($entry->contact_phone),
            "admin_email"    => strval($entry->contact_email),
            "admin_name"     => "",
            "ip_address"     => $ip,
            "hostname"       => $hostname,
            "pbx_type"       => strval($entry->type),
            "created_at"     => array("$date" => "2024-03-13T17:00:00.000Z"),
            "trunks"         => array(),
            "office_codes"   => array(array(
                "office_code"    => $office_code,
                "npa"            => strval($entry->npa),
                "dialplans"      => array(),
            )),
        );

        $office_code_index[$office_code] = $index++;
    }

    // find all the dialplans in the XML file
    echo ";;;\tDiscoveing dialplans and attaching to office codes\n";
    $rejected_dialplans = array();
    foreach ($xml->dialplan as $entry) {
        echo "Adding " . $entry->translation . "\n";
        $office_code = strval($entry->serving_office);
        if (array_key_exists($office_code, $office_code_index)) {
            array_push($pbxes[$office_code_index[$office_code]]["office_codes"][0]["dialplans"],
                strval($entry->translation));
        } else {
            echo "; ERROR: office code " . $office_code . " not defined in XML file.\n";
            array_push($rejected_dialplans, $office_code . ": " . $entry->translation);
        }
    }

    // find all the trunks in the XML file
    echo ";;;\tDiscovering trunks between office codes\n";
    $rejected_trunks = array();
    foreach ($xml->trunks as $entry) {
        $originating_office = strval($entry->office_code_a);
        $terminating_office = strval($entry->office_code_z);
        if (array_key_exists($originating_office, $office_code_index)) {
            array_push(
                $pbxes[$office_code_index[$originating_office]]["trunks"],
                array('office_code' => $terminating_office));
        } else {
            echo "; ERROR: office code " . $originating_office . " not defined in XML file.\n";
            array_push($rejected_trunks, $originating_office . " -> " . $terminating_office);
        }
    }

    // Write all the data out as a JSON document
    $doc = fopen("mongodb_import.json", "w");
    fwrite($doc, json_encode($pbxes));
    fclose($doc);

    $doc = fopen("rejected_dialplans.txt", "w");
    fwrite($doc, implode("\n", $rejected_dialplans));
    fwrite($doc, "\n");
    fclose($doc);

    $doc = fopen("rejected_trunks.txt", "w");
    fwrite($doc, implode("\n", $rejected_trunks));
    fwrite($doc, "\n");
    fclose($doc);
}
