
<?php

// Load XML file
// $contents = file_get_contents("http://n2mh-web.local.mesh/mpadmin/meshphone_xml.xml");
// if ($contents == "") {
//     # if we got nothing, then signal that file should not be replaced
//     exit(1);
// }
// $xml = simplexml_load_string($contents);

// echo "create constraint on (p: pbx) assert p.office is unique; \n";

// Load XML file (override with local meshphone_xml.xml for testing)
if (file_exists("network.json")) {
    $pbxes = json_decode(file_get_contents("network.json"));
} else {
    $contents = file_get_contents("http://wt0f-meshphone.local.mesh/network_json");
    if ($contents == "") {
        # if we got nothing, then signal that file should not be replaced
        exit(1);
    }
    $pbxes = json_decode($contents);
}

if ($pbxes) {

    echo "match (n) detach delete n;\n";

    foreach ($pbxes as $entry) {
        $callsign = strval($entry->admin_callsign);
        $phone = strval($entry->admin_phone);
        $email = strval($entry->admin_email);
        $pbx_name = strval($entry->name);
        if ((int)$pbx_name > 0) {
            $pbx_name = "a$pbx_name";
        }
        $admin_name = strval($entry->admin_name);
        $location = strval($entry->location);
        $hostname = strval($entry->hostname);
        $ip_address = strval($entry->ip_address);
        $pbx_type = strval($entry->pbx_type);
        $pbx_status = strval($entry->pbx_status);
        echo <<<END
        create ($pbx_name:PBX {pbx_name: "$pbx_name", admin_name: "$admin_name", admin_callsign: "$callsign", admin_phone: "$phone", admin_email: "$email", location: "$location", hostname: "$hostname", ip_address: "$ip_address", pbx_type: "$pbx_type", pbx_status: "$pbx_status"} );

        END;

        foreach ($entry->office_codes as $office) {
            $code = strval($office->office_code);
            $npa = strval($office->npa);
            echo <<<END
            create (office_$code:Office {office_code: "$code", npa: "$npa"});
            match (p:PBX {pbx_name: "$pbx_name"})
            match (o:Office {office_code: "$code"})
            create (o)-[:hosted_on]->(p);

            END;

            foreach ($office->dialplans as $dialplan) {
                $pattern = strval($dialplan);
                $id = strtr(str_replace(' ', '', $pattern), '[]-', '___');
                echo <<<END
                create (plan_$id:Dialplan {pattern: "$pattern"});
                match (o:Office {office_code: "$code"})
                match (d:Dialplan {pattern: "$pattern"})
                create (d)-[:rule]->(o);

                END;
            }
        }
    }

    // have to wait until all PBX info is recorded befor creating trunks
    $seen = array();
    foreach ($pbxes as $entry) {
        $pbx_name = strval($entry->name);
        $network = strval($entry->network) || "";
        foreach ($entry->trunks as $trunk) {
            $remote_code = strval($trunk->office_code);
            $remote_name = strval($trunk->pbx_name);

            if (! in_array($remote_name . "-" . $pbx_name, $seen)) {
                echo <<<END
                match (o:PBX {pbx_name: "$pbx_name"})
                match (t:PBX {pbx_name: "$remote_name"})
                create (o)-[:trunk {network: "$network"}]->(t);

                END;

                array_push($seen, $pbx_name . "-" . $remote_name);
            }
        }
    }
}
